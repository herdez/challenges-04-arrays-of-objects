/** 

You must generate a function that takes as parameter a `string` 
that contains a meal, and look for it in the next object 'foodGroups', 
returning its` key` and its english value in a new object, 
if it does not find the food it must return "food not found".
**/

// ++ Write YOUR CODE Below





// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

var foodGroups = {
	grano: [{name: {spanish: 'Arroz', english: 'rice'}, type: 'grano'},{name: {spanish: 'Trigo', english: 'wheat'}, type: 'grano'}, {name: {spanish: 'Avena', english: 'oats'}, type: 'grano'}, {name: {spanish: 'Cebada', english: 'barley'}, type: 'grano'}, {name: {spanish: 'Harina', english: 'flour'}, type: 'grano'}],
	vegetal: [{name: {spanish: 'Zanahoria', english: 'carrot'}, type: 'vegetal'}, {name: {spanish: 'Maiz', english: 'corn'}, type: 'vegetal'}, {name: {spanish: 'Elote', english: 'corn cob'}, type: 'vegetal'}, {name: {spanish: 'Calabaza', english: 'pumpkin'}, type: 'vegetal'}, {name: {spanish: 'Papa', english: 'pope'}, type: 'vegetal'}],
	fruta: [{name: {spanish: 'Manzana', english: 'apple'}, type: 'fruta'}, {name: {spanish: 'Mango', english: 'mango'}, type: 'fruta'}, {name: {spanish: 'Fresa', english: 'strawberry'}, type: 'fruta'}, {name: {spanish: 'Durazno', english: 'peach'}, type: 'fruta'}, {name: {spanish: 'Piña', english: 'pineapple'}, type: 'fruta'}],
	carne: [{name: {spanish: 'Res', english: 'beef'}, type: 'carne'}, {name: {spanish: 'Pollo', english: 'chicken'}, type: 'carne'}, {name: {spanish: 'Salmon', english: 'salmon'}, type: 'carne'}, {name: {spanish: 'Pescado', english: 'fish'}, type: 'carne'}, {name: {spanish: 'Cerdo', english: 'pork'}, type: 'carne'}],
	lacteo: [{name: {spanish: 'Leche', english: 'milk'}, type: "lacteo"}, {name: {spanish: 'Yogur', english: 'yogurt'}, type: "lacteo"}, {name: {spanish: 'Queso', english: 'cheese'}, type: 'lacteo'}, {name: {spanish: 'Crema', english: 'cream'}, type: 'lacteo'}]
	}



/*-------------------TEST-1-------------------------*/

console.log("==== ex-02-foodGroups : TEST 1 ====");

  
console.assert(foodGroup('Crema').lacteo.english === 'cream');
console.assert(foodGroup('Res').carne.english === 'beef');
console.assert(foodGroup('Piña').fruta.english === 'pineapple');

/*-------------------TEST-2-------------------------*/

console.log("==== ex-02-foodGroups : TEST 2 ====");

console.assert(foodGroup('Caña') === "Food not found");

/*--------------------------------------------------*/
/*-------------------END----------------------------*/
console.log('\n\n');

